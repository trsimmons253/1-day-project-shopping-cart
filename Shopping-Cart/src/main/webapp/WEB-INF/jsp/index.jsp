<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to ShoppingCentral!</title>
</head>
<body>
	<header> 
		<a href="checkShoppingCart"><button>Check Shopping Cart</button></a>
		<hr noshade size="3">
	</header>
	<h1>${ itemConfirmation }</h1>
	<h1>Items</h1>
	<c:forEach items="${ items }" var="item">
		<section>
			<form method="POST" action="addToCart">
				<label>${ item.itemName }</label> <br>
				<label>${ item.itemDescription }</label> <br>
				<label>$${ item.itemPrice }</label> <br>
				<input type="hidden" name="itemId" value="${ item.itemId }"/>
				<input type="number" name="quantity" min="1" max="100">
				<input type="submit" value="Add to Cart" /> <br> <br>
			</form>
		</section>
	</c:forEach>
</body>
</html>