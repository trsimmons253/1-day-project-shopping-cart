<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to ShoppingCentral!</title>
</head>
<body>
	<h1>Items</h1>
	<c:forEach items="${ items }" var="item">
		<section>
			<form method="post" action="#">
				<label>${ item.itemName }</label> <br>
				<p>${ item.itemDescription }</p> <br>
				<label>${ item.itemPrice }</label> <br>
				<input type="hidden" name="itemId" value="${ item.itemId }"/>
				<select>
					<?php
   						for ($i=1; $i<=100; $i++)
   						{
        			?>
            			<option value="<?php echo $i;?>"><?php echo $i;?></option>
        			<?php
    					}
					?>
				</select>
				<input type="submit" value="Add to Cart" />
			</form>
		</section>
	</c:forEach>
</body>
</html>