<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:forEach items="${ cartItems }" var="cartItem">
		<c:forEach items="${ items }" var="item">
			<c:if test="${ cartItem.itemId == item.itemId }">
				<h2>${ item.itemName }</h2> <br>
				<h3>${ item.itemDescription }</h3> <br>
				<h3>${ cartItem.quantity }x $${ item.itemPrice }</h3> <br>
			</c:if>
		</c:forEach>
	</c:forEach>
	<h2>Total: ${ total }</h2>
</body>
</html>