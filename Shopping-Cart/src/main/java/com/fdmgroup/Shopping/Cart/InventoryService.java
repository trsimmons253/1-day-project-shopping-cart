package com.fdmgroup.Shopping.Cart;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryService {

	@Autowired
	private InventoryRepository inventoryRepo;
	
	public void save(Inventory inventory) {
		inventoryRepo.save(inventory);
	}

	public Inventory find(long itemId) {
		Optional<Inventory> inventory = inventoryRepo.findById(itemId);
		if(inventory.isPresent()) {
			return inventory.get();
		}
		return null;
	}

	public void update(Inventory inventory) {
		inventoryRepo.save(inventory);
	}

}
