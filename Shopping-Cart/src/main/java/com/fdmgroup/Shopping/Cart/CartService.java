package com.fdmgroup.Shopping.Cart;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartService {

	@Autowired
	private CartRepository cartRepo;
	
	public void save(ShoppingCart cart) {
		cartRepo.save(cart);
	}

	public ShoppingCart find(long id) {
		Optional<ShoppingCart> cart = cartRepo.findById(id);
		if(cart.isPresent()) {
			return cart.get();
		}
		return null;
	}

}
