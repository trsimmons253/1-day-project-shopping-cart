package com.fdmgroup.Shopping.Cart;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {

	@Query("SELECT c FROM CartItem c WHERE c.shoppingCart = ?1")
	Set<CartItem> findWithCart(ShoppingCart cart);

}
