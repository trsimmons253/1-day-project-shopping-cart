package com.fdmgroup.Shopping.Cart;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ItemService {

	@Autowired
	private ItemRepository itemRepo;
	
	public void save(Item item) {
		itemRepo.save(item);
	}

	public Item find(long itemId) {
		Optional<Item> item = itemRepo.findById(itemId);
		if(item.isPresent()) {
			return item.get();
		}
		return null;
	}

	public List<Item> getAll() {
		List<Item> items = itemRepo.findAll();
		return items;
	}

}
