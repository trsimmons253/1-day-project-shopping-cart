package com.fdmgroup.Shopping.Cart;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartItemService {

	@Autowired
	CartItemRepository cartItemRepo;

	public Set<CartItem> find(ShoppingCart cart) {
		Set<CartItem> cartItems = new HashSet<>();
		cartItems = cartItemRepo.findWithCart(cart);
		return cartItems;
	}

	public void save(CartItem cartItem) {
		cartItemRepo.save(cartItem);
	}

}
