package com.fdmgroup.Shopping.Cart;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Component
@Scope("prototype")
@Table(name = "inventory")
public class Inventory {

	@Id
	@Column(name = "item_id")
	private long itemId;
	private long quantity;
	
	public Inventory() {
		
	}
	
	public long getItemId() {
		return itemId;
	}
	
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	
	public long getQuantity() {
		return quantity;
	}
	
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (itemId ^ (itemId >>> 32));
		result = prime * result + (int) (quantity ^ (quantity >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Inventory))
			return false;
		Inventory other = (Inventory) obj;
		if (itemId != other.itemId)
			return false;
		if (quantity != other.quantity)
			return false;
		return true;
	}

	public void updateQuantity(long quantity) {
		this.quantity -= quantity;
	}
	
}
