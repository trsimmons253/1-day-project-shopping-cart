package com.fdmgroup.Shopping.Cart;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Entity
@Component
@Scope("prototype")
@Table(name = "shopping_cart")
public class ShoppingCart {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "shopping_cart_generator")
	@SequenceGenerator(name = "shopping_cart_generator", sequenceName = "shopping_cart_id_seq")
	@Column(name = "cart_id")
	private long id;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "shoppingCart", cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	private Set<CartItem> items = new HashSet<>();

	public ShoppingCart() {
		
	}
	
	public void addItem(CartItem cartItem) {
		items.add(cartItem);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Set<CartItem> getItems() {
		return items;
	}

	public void setItems(Set<CartItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ShoppingCart))
			return false;
		ShoppingCart other = (ShoppingCart) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
