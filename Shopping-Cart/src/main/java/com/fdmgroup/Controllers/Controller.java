package com.fdmgroup.Controllers;

import java.math.BigDecimal;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.fdmgroup.Shopping.Cart.CartItem;
import com.fdmgroup.Shopping.Cart.CartItemService;
import com.fdmgroup.Shopping.Cart.CartService;
import com.fdmgroup.Shopping.Cart.Inventory;
import com.fdmgroup.Shopping.Cart.InventoryService;
import com.fdmgroup.Shopping.Cart.Item;
import com.fdmgroup.Shopping.Cart.ItemService;
import com.fdmgroup.Shopping.Cart.ShoppingCart;

@org.springframework.stereotype.Controller
public class Controller {

	@Resource
	private ApplicationContext ctx;
	
	@Resource
	private CartItemService cartItemService;
	@Resource
	private CartService cartService;
	@Resource
	private InventoryService inventoryService;
	@Resource
	private ItemService itemService;
	
	@GetMapping("/")
	public String index(ModelMap model, HttpServletRequest req) {
		model.addAttribute("items", itemService.getAll());
		return "index";
	}
	
	@PostMapping("/addToCart")
	public String addToCart(ModelMap model, HttpServletRequest req) {
		ShoppingCart cart = cartService.find(1L);
		String tempId = req.getParameter("itemId");
		long itemId = Long.parseLong(tempId);
		String tempQuantity = req.getParameter("quantity");
		long quantity = Long.parseLong(tempQuantity);
		CartItem cartItem = new CartItem(itemId, quantity, cartService.find(1L));
		cart.addItem(cartItem);
		
		Inventory inventory = inventoryService.find(itemId);
		if ((inventory.getQuantity() - quantity) >= 0) {
//			cartItem.setItemId(itemId);
//			cartItem.setQuantity(quantity);
//			cartItem.setShoppingCart(cart);
//			cart.addItem(cartItem);
			cartService.save(cart);
			cartItemService.save(cartItem);
			inventory.updateQuantity(quantity);
			inventoryService.update(inventory);
			model.addAttribute("itemConfirmation", "Your item was successfully added to your shopping cart.");
		}
		else 
		{
			model.addAttribute("itemConfirmation", "There is not enough of your selected item in stock.");
		}
		
		model.addAttribute("items", itemService.getAll());
		return "index";
	}
	
	@GetMapping("/checkShoppingCart")
	public String checkShoppingCart(ModelMap model) {
		ShoppingCart cart = cartService.find(1L);
		Set<CartItem> cartItems = cartItemService.find(cart);
		BigDecimal total = new BigDecimal("0");
		for (CartItem c : cartItems) {
			Item item = itemService.find(c.getItemId());
			total = total.add(item.getItemPrice());
		}
		model.addAttribute("cartItems", cartItems);
		model.addAttribute("items", itemService.getAll());
		model.addAttribute("total", total);
		return "shoppingCart";
	}
	
}
