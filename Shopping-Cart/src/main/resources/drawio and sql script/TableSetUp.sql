CREATE TABLE shopping_cart (
  CART_ID NUMBER(19,0) NOT NULL,
  PRIMARY KEY (CART_ID)
);

CREATE TABLE cart_items (
  ITEM_ID NUMBER(19,0) NOT NULL,
  QUANTITY NUMBER(19,0) NOT NULL,
  SHOPPING_CART_CART_ID NUMBER(19,0),
  PRIMARY KEY (ITEM_ID),
  FOREIGN KEY (SHOPPING_CART_CART_ID) REFERENCES shopping_cart(CART_ID)
);

CREATE TABLE store_items (
  ITEM_ID NUMBER(19,0) NOT NULL,
  ITEM_DESCRIPTION VARCHAR2(255 CHAR),
  ITEM_NAME VARCHAR2(255 CHAR),
  ITEM_PRICE NUMBER(19,2),
  PRIMARY KEY (ITEM_ID)
);

CREATE TABLE inventory (
  ITEM_ID NUMBER(19,0) NOT NULL,
  QUANTITY NUMBER(19,0) NOT NULL,
  PRIMARY KEY (ITEM_ID)
);

INSERT INTO shopping_cart (item_id)
VALUES (1);

INSERT INTO store_items (item_id, item_description, item_name, item_price)
VALUES (1, '1 lb of thick sliced bacon', 'Bacon', 3.58);

INSERT INTO store_items (item_id, item_description, item_name, item_price)
VALUES (2, '1 gallon of whole milk', 'Milk', 1.59);

INSERT INTO store_items (item_id, item_description, item_name, item_price)
VALUES (3, 'One dozen eggs', 'Eggs', 0.81);

INSERT INTO store_items (item_id, item_description, item_name, item_price)
VALUES (4, '1 lb of chicken breast', 'Chicken', 5.47);

INSERT INTO store_items (item_id, item_description, item_name, item_price)
VALUES (5, 'Frozen pepperoni pizza', 'Pizza', 3.51);

INSERT INTO inventory (item_id, quantity)
VALUES (1, 50);

INSERT INTO inventory (item_id, quantity)
VALUES (2, 50);

INSERT INTO inventory (item_id, quantity)
VALUES (3, 30);

INSERT INTO inventory (item_id, quantity)
VALUES (4, 30);

INSERT INTO inventory (item_id, quantity)
VALUES (5, 25);

