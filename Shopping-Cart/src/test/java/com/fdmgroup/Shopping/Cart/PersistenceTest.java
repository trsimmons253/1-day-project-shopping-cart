package com.fdmgroup.Shopping.Cart;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@WebAppConfiguration
public class PersistenceTest {

	@Autowired
	private ItemService itemService;
	@Autowired
	private CartService cartService;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private CartItemService cartItemService;
	
	@Autowired
	private ApplicationContext ctx;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void test_AddItemsAndInventoryToDatabase() {
		Item item = ctx.getBean(Item.class);
		Inventory inventory = ctx.getBean(Inventory.class);
		item.setItemName("Bacon");
		item.setItemDescription("1lb of thick sliced bacon");
		item.setItemPrice(new BigDecimal("3.58"));
		itemService.save(item);
		inventory.setItemId(item.getItemId());
		inventory.setQuantity(50L);
		inventoryService.save(inventory);
		assertEquals(item, itemService.find(item.getItemId()));
		assertEquals(inventory, inventoryService.find(inventory.getItemId()));
	}
	
	@Test
	public void test_SearchForInvalidItemId_ReturnsNull() {
		Item item = itemService.find(999L);
		Inventory inventory = inventoryService.find(999L);
		assertNull(inventory);
		assertNull(item);
	}
	
	@Test
	public void test_AddShoppingCartToDatabase() {
		Item item = new Item();
		Inventory inventory = new Inventory();
		ShoppingCart cart = new ShoppingCart();
		CartItem cartItem = new CartItem();
		item.setItemName("Milk");
		item.setItemDescription("1 gallon of whole milk");
		item.setItemPrice(new BigDecimal("1.59"));
		itemService.save(item);
		inventory.setItemId(item.getItemId());
		inventory.setQuantity(50L);
		inventoryService.save(inventory);
		cartItem.setItemId(item.getItemId());
		cartItem.setQuantity(1L);
		cartItem.setShoppingCart(cart);
		Inventory i = inventoryService.find(item.getItemId());
		i.updateQuantity(cartItem.getQuantity());
		inventoryService.save(i);
		Set<CartItem> cartItems = new HashSet<>();
		cartItems.add(cartItem);
		cart.setItems(cartItems);
		cartService.save(cart);
		cartItemService.save(cartItem);
		for (CartItem c : cartItemService.find(cart)) {
			assertEquals(cartItem, c);
		}
//		assertEquals(cartItems, cartItemService.find(cart));
	}
	
	@Test
	public void test_addShoppingCartToDatabase() {
		ShoppingCart cart = ctx.getBean(ShoppingCart.class);
//		CartItem cartItem = ctx.getBean(CartItem.class);
//		cartItem.setItemId(100);
//		cartItem.setQuantity(100L);
//		cart.addItem(cartItem);
		cartService.save(cart);
		assertNotNull(cart);
	}

}
